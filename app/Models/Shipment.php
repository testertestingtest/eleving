<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'order_id',
        'courier_id',
        'shipment_status_id',
        'sending_address_id',
        'receiving_address_id',
        'distance',
        'rate',
        'price',
        'delivery_till',
        'delivered_at'
    ];

    public function status()
    {
        return $this->belongsTo(ShipmentStatus::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function courier()
    {
        return $this->belongsTo(User::class, 'courier_id');
    }

    public function sendingAddress()
    {
        return $this->belongsTo(Address::class, 'sending_address_id');
    }

    public function receivingAddress()
    {
        return $this->belongsTo(Address::class, 'receiving_address_id');
    }
}

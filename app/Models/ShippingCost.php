<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingCost extends Model
{
    use HasFactory;

    public function calculate(array $params = array()): self
    {
        $this->user_id = $params['user_id'];
        $this->sending_address_id = $params['sending_address_id'];
        $this->receiving_address_id = $params['receiving_address_id'];
        $this->delivery_till = $params['delivery_till'];

        $this->distance = ($params['sending_address_id'] + $params['receiving_address_id']) * 2;
        $this->rate = ($params['sending_address_id'] + $params['receiving_address_id']) * 2;
        $this->price = (($params['sending_address_id'] + $params['receiving_address_id']) * 2) + 2;

        return $this;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Validation\ValidationException;

use App\Models\Order;
use App\Http\Resources\OrderResource;
use App\Http\Requests\GetOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\DeleteOrderRequest;
use App\Http\Requests\ListOrderRequest;

class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\ListOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ListOrderRequest $request)
    {
        $orders = OrderResource::collection(Order::all());
        return $this->sendResponse($orders, 'I18N_ORDERS_FETCHED_SUCCESSFULLY');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrderRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreOrderRequest $request)
    {
         try {
            $validated = $request->validated();
         } catch (ValidationException $e) {
             return $this->sendError($e->errors());
         }

        $order = Order::create($validated);

        return $this->sendResponse(new OrderResource($order), 'I18N_ORDER_CREATED_SUCCESSFULLY');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Http\Requests\GetOrderRequest $request
     * @param  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(GetOrderRequest $request, $id)
    {
        $order = Order::findOrFail($id);
        if (is_null($order)) {
            return $this->sendError('I18N_ORDER_DOES_NOT_EXIST');
        }

        return $this->sendResponse(new OrderResource($order), 'I18N_ORDER_FETCHED_SUCCESSFULLY');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOrderRequest $request
     * @param  \App\Models\Order $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {
        try {
            $validated = $request->validated();
        } catch (ValidationException $e) {
            return $this->sendError($e->errors());
        }

        $order->update($validated);

        return $this->sendResponse(new OrderResource($order), 'I18N_ORDER_UPDATED_SUCCESSFULLY');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Http\Requests\DeleteOrderRequest $request
     * @param  \App\Models\Order $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteOrderRequest $request, Order $order)
    {
        $order->delete();
        return $this->sendResponse([], 'I18N_ORDER_DELETED_SUCCESSFULLY');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Validation\ValidationException;

use App\Http\Resources\ShipmentResource;
use App\Http\Requests\ListShipmentRequest;
use App\Http\Requests\GetShipmentRequest;
use App\Http\Requests\StoreShipmentRequest;
use App\Http\Requests\UpdateShipmentRequest;
use App\Http\Requests\DeleteShipmentRequest;
use App\Models\Shipment;

class ShipmentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\ListShipmentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ListShipmentRequest $request)
    {
        $orders = ShipmentResource::collection(Shipment::all());
        return $this->sendResponse($orders, 'I18N_SHIPMENTS_FETCHED_SUCCESSFULLY');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreShipmentRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreShipmentRequest $request)
    {
        try {
            $validated = $request->validated();
        } catch (ValidationException $e) {
            return $this->sendError($e->errors());
        }

        $shipment = Shipment::create($validated);

        return $this->sendResponse(new ShipmentResource($shipment), 'I18N_SHIPMENT_CREATED_SUCCESSFULLY');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Http\Requests\GetShipmentRequest $request
     * @param  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(GetShipmentRequest $request, $id)
    {
        $shipment = Shipment::findOrFail($id);
        if (is_null($shipment)) {
            return $this->sendError('I18N_SHIPMENT_DOES_NOT_EXIST');
        }

        return $this->sendResponse(new ShipmentResource($shipment), 'I18N_SHIPMENT_FETCHED_SUCCESSFULLY');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateShipmentRequest $request
     * @param  \App\Models\Shipment $shipment
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateShipmentRequest $request, Shipment $shipment)
    {
        try {
            $validated = $request->validated();
        } catch (ValidationException $e) {
            return $this->sendError($e->errors());
        }

        $shipment->update($validated);

        return $this->sendResponse(new ShipmentResource($shipment), 'I18N_SHIPMENT_UPDATED_SUCCESSFULLY');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Http\Requests\DeleteShipmentRequest $request
     * @param  \App\Models\Shipment $shipment
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteShipmentRequest $request, Shipment $shipment)
    {
        $shipment->delete();
        return $this->sendResponse([], 'I18N_SHIPMENT_DELETED_SUCCESSFULLY');
    }
}

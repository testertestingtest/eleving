<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreShippingCostRequest;
use App\Http\Resources\ShippingCostResource;
use App\Models\ShippingCost;
use Illuminate\Validation\ValidationException;

class ShippingCostController extends BaseController
{
    /**
     * Calculate shipping cost based on sender and receiver address
     *
     * An HTTP POST doesn't necessarily have to create a persisted resource. In RFC 2616, section 9.5
     * (https://www.rfc-editor.org/rfc/rfc2616#section-9.5)
     *
     * @param StoreShippingCostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreShippingCostRequest $request, ShippingCost $shippingCost)
    {
        try {
            $validated = $request->validated();
        } catch (ValidationException $e) {
            return $this->sendError($e->errors());
        }

        return $this->sendResponse(new ShippingCostResource($shippingCost->calculate($validated)), 'I18N_SHIPPING_COST_CREATED_SUCCESSFULLY');
    }
}

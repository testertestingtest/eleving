<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;
use App\Models\User;

class AuthController extends BaseController
{
    /**
     * Login existing user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signIn(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $authUser = Auth::user();
            $success['token'] =  $authUser->createToken(env('APP_NAME'))->plainTextToken;
            $success['name'] =  $authUser->name;

            return $this->sendResponse($success, 'I18N_USER_SIGNED_IN_SUCCESSFULLY');
        } else {
            return $this->sendError('I18N_UNAUTHORISED', ['error' => 'I18N_UNAUTHORISED']);
        }
    }

    /**
     * Create new user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signUp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);

        if($validator->fails()) {
            return $this->sendError('I18N_VALIDATION_ERROR', $validator->errors());
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken(env('APP_NAME'))->plainTextToken;
        $success['name'] =  $user->name;

        return $this->sendResponse($success, 'I18N_USER_CREATED_SUCCESSFULLY');
    }
}

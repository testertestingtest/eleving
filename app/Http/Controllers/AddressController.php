<?php

namespace App\Http\Controllers;

use Illuminate\Validation\ValidationException;

use App\Http\Requests\ListAddressRequest;
use App\Http\Requests\DeleteAddressRequest;
use App\Http\Requests\GetAddressRequest;
use App\Http\Requests\StoreAddressRequest;
use App\Http\Requests\UpdateAddressRequest;
use App\Http\Resources\AddressResource;
use App\Models\Address;

class AddressController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\ListAddressRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ListAddressRequest $request)
    {
        $addresses = AddressResource::collection(Address::all());
        return $this->sendResponse($addresses, 'I18N_ADDRESSES_FETCHED_SUCCESSFULLY');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAddressRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreAddressRequest $request)
    {
        try {
            $validated = $request->validated();
        } catch (ValidationException $e) {
            return $this->sendError($e->errors());
        }

        $address = Address::create($validated);

        return $this->sendResponse(new AddressResource($address), 'I18N_ADDRESS_CREATED_SUCCESSFULLY');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(GetAddressRequest $request, $id)
    {
        $address = Address::findOrFail($id);
        if (is_null($address)) {
            return $this->sendError('I18N_ADDRESS_DOES_NOT_EXIST');
        }

        return $this->sendResponse(new AddressResource($address), 'I18N_ADDRESS_FETCHED_SUCCESSFULLY');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAddressRequest $request
     * @param  \App\Models\Address $address
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateAddressRequest $request, Address $address)
    {
        try {
            $validated = $request->validated();
        } catch (ValidationException $e) {
            return $this->sendError($e->errors());
        }

        $address->update($validated);

        return $this->sendResponse(new AddressResource($address), 'I18N_ADDRESS_UPDATED_SUCCESSFULLY');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Http\Requests\DeleteAddressRequest $request
     * @param Address $address
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteAddressRequest $request, Address $address)
    {
        $address->delete();

        return $this->sendResponse([], 'I18N_ADDRESS_DELETED_SUCCESSFULLY');
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreShippingCostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole(['admin', 'sales', 'customer']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer|exists:App\Models\User,id',
            'sending_address_id' => 'required|integer|exists:App\Models\Address,id',
            'receiving_address_id' => 'required|integer|exists:App\Models\Address,id',
            'delivery_till' => 'required|date_format:Y-m-d H:i:s',
        ];
    }
}

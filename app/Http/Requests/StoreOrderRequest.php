<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole(['admin', 'sales', 'customer']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id'  => 'required|integer|exists:App\Models\User,id',
            'seller_id' => 'required|integer|exists:App\Models\User,id',
            'order_status_id' => 'required|integer|exists:App\Models\OrderStatus,id',
            'tax' => 'required|numeric',
            'total'  => 'required|numeric',
        ];
    }
}

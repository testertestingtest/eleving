<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateShipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole(['admin', 'courier']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'  => 'integer|exists:App\Models\Order,id',
            'courier_id' => 'integer|exists:App\Models\User,id',
            'shipment_status_id' => 'integer|exists:App\Models\ShipmentStatus,id',
            'sending_address_id' => 'integer|exists:App\Models\Address,id',
            'receiving_address_id' => 'integer|exists:App\Models\Address,id',
            'distance' => 'numeric',
            'rate' => 'numeric',
            'price' => 'numeric',
            'delivery_till' => 'date_format:Y-m-d H:i:s',
            'delivered_at' => 'date_format:Y-m-d H:i:s',
        ];
    }
}

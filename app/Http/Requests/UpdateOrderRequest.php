<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole(['admin', 'sales']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id'  => 'integer|exists:App\Models\User,id',
            'seller_id' => 'integer|exists:App\Models\User,id',
            'order_status_id' => 'integer|exists:App\Models\OrderStatus,id',
            'tax' => 'numeric',
            'total' => 'numeric',
        ];
    }
}

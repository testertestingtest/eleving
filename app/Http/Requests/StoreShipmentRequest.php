<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreShipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole(['admin', 'sales']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'  => 'required|integer|exists:App\Models\Order,id',
            'courier_id' => 'required|integer|exists:App\Models\User,id',
            'shipment_status_id' => 'required|integer|exists:App\Models\ShipmentStatus,id',
            'sending_address_id' => 'required|integer|exists:App\Models\Address,id',
            'receiving_address_id' => 'required|integer|exists:App\Models\Address,id',
            'distance' => 'required|numeric',
            'rate' => 'required|numeric',
            'price' => 'required|numeric',
            'delivery_till' => 'required|date_format:Y-m-d H:i:s',
        ];
    }
}

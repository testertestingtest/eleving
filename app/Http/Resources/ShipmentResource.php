<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShipmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'courier_id' => $this->courier_id,
            'shipment_status_id' => $this->shipment_status_id,
            'sending_address_id' => $this->sending_address_id,
            'receiving_address_id' => $this->receiving_address_id,
            'distance' => $this->distance,
            'rate' => $this->rate,
            'price' => $this->price,
            'delivery_till' => $this->delivery_till,
            'delivered_at' => $this->delivered_at,
            'order' => $this->order()->get()->toArray()
        ];
    }
}

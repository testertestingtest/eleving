<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'seller_id' => $this->seller_id,
            'customer_id' => $this->customer_id,
            'order_status_id' => $this->order_status_id,
            'tax' => $this->tax,
            'total' => $this->total,
            'products' => $this->products()->get()->toArray(),
        ];
    }
}

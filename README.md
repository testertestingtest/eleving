# API for the delivery service in PHP

## Table of Contents
- [Database Diagram](#database-diagram)
- [Installation](#finstallation)
- [Routes](#routes)
    - [Addresses](#addresses)
    - [Authentication](#authentication)
    - [Orders](#orders)
    - [Shipments](#shipments)
    - [Shipping Costs](#shipping-costs)

## Database diagram

<img src="./readme/model.png" alt="Database diagram">

## Installation

1. Clone repository
````
$ git clone https://gitlab.com/testertestingtest/eleving.git
````
2. Enter folder
````   
$ cd eleving
````
3. Install composer dependencies
````
~/eleving$ composer install
````
4. Generate APP_KEY
````
~/eleving$ php artisan key:generate
````
5. Configure .env file, edit file with next command $ nano .env
````
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=database
DB_USERNAME=user
DB_PASSWORD=secret
````
6. Run migrations
````
~/eleving$ php artisan migrate
````
### Routes

##### Addresses

- GET /api/addresses
- POST /api/addresses
- GET /api/addresses/{id}
- DELETE /api/addresses/{id}
- PUT /api/addresses/{id}

##### Authentication

- POST /api/login
- POST /api/register

##### Orders

- POST /api/order/
- GET /api/orders/
- POST /api/orders/{id}
- GET /api/orders/{id}
- DELETE /api/orders/{id}

##### Shipments

- POST /api/shipment/
- GET /api/shipments/
- POST /api/shipments/{id}
- GET /api/shipments/{id}
- DELETE /api/shipments/{id}

##### Shipping cost

- POST /api/shipping_costs

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).

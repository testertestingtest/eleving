<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Models\User;
use App\Models\ShippingCost;

use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ShippingCostApiTest extends TestCase
{
    /**
     * Retrieve ShippingCost mockup data
     *
     * @param array $params
     * @return string[]
     */
    protected function getShippingCostData(array $params = array()): array
    {
        return [
            'distance' => ($params['sending_address_id'] + $params['receiving_address_id']) * 2,
            'rate' => ($params['sending_address_id'] + $params['receiving_address_id']) * 2,
            'price' => (($params['sending_address_id'] + $params['receiving_address_id']) * 2) + 2,
        ];
    }
    /**
     * Return user with customer role
     *
     * @return User
     */
    protected function getCustomerUser(): User
    {
        $user = User::factory()->create();
        $role = Role::create(['name' => 'customer']);
        $user->assignRole([$role->id]);

        return $user;
    }

    /**
     * Check if expected shippingCost can be crated
     *
     * @return void
     */
    public function test_shipping_cost_create()
    {
        $user = $this->getCustomerUser();

        $sendingAddress = Address::factory()->create();
        $receivingAddress = Address::factory()->create();

        $expected = [
            'data' => $this->getShippingCostData([
                'sending_address_id' => $sendingAddress->id,
                'receiving_address_id' => $receivingAddress->id
            ]),
            'success' => true,
            'message' => 'I18N_SHIPPING_COST_CREATED_SUCCESSFULLY'
        ];

        $payload = [
            'user_id' => $user->id,
            'sending_address_id' => $sendingAddress->id,
            'receiving_address_id' => $receivingAddress->id,
            'delivery_till' => now()->toDateTimeString()
        ];

        $expected['data'] = array_merge($expected['data'], $payload);

        $response = $this->actingAs($user)->post("/api/shipping_costs", $payload);
        $response->assertStatus(200);

        $responseData = $response->json();

        $this->assertEquals($expected, $responseData);
    }
}

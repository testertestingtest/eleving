<?php

namespace Tests\Feature;

use App\Models\Shipment;
use App\Models\ShipmentStatus;
use App\Models\User;
use App\Models\Order;
use App\Models\Address;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ShipmentApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Retrieve Shipment mockup data
     *
     * @param Shipment $shipment
     * @return string[]
     */
    protected function getShipmentData(Shipment $shipment): array
    {
        return [
            'id' => $shipment->id,
            'order_id' => $shipment->order_id,
            'courier_id' => $shipment->courier_id,
            'shipment_status_id' => $shipment->shipment_status_id,
            'sending_address_id' => $shipment->sending_address_id,
            'receiving_address_id' => $shipment->receiving_address_id,
            'distance' => $shipment->distance,
            'rate' => $shipment->rate,
            'price' => $shipment->price,
            'delivery_till' => $shipment->delivery_till,
            'delivered_at' => $shipment->delivered_at,
            'order' => $shipment->order()->get()->toArray()
        ];
    }

    /**
     * Return user with courier role
     *
     * @return User
     */
    protected function getCourierUser(): User
    {
        $user = User::factory()->create();
        $role = Role::create(['name' => 'courier']);
        $user->assignRole([$role->id]);

        return $user;
    }

    /**
     * Return user with admin role
     *
     * @return User
     */
    protected function getAdminUser(): User
    {
        $user = User::factory()->create();
        $role = Role::create(['name' => 'admin']);
        $user->assignRole([$role->id]);

        return $user;
    }

    /**
     * Check if expected shipment data is the same as in API
     *
     * @return void
     */
    public function test_shipment_list()
    {
        $user = $this->getCourierUser();

        $expected = [
            'data' => [],
            'success' => true,
            'message' => 'I18N_SHIPMENTS_FETCHED_SUCCESSFULLY',
        ];

        $shipments = Shipment::factory()
            ->count(10)
            ->create();

        foreach($shipments as $shipment) {
            $expected['data'][] = $this->getShipmentData($shipment);
        }

        $response = $this->actingAs($user)->get('/api/shipments');
        $response->assertStatus(200);

        $this->assertEquals($expected, $response->json());
    }

    /**
     * Check if expected order data is the same as in API
     *
     * @return void
     */
    public function test_shipment_get()
    {
        $user = $this->getCourierUser();

        $shipment = Shipment::factory()->create();

        $expected = [
            'data' => $this->getShipmentData($shipment),
            'success' => true,
            'message' => 'I18N_SHIPMENT_FETCHED_SUCCESSFULLY'
        ];

        $response = $this->actingAs($user)->get("/api/shipments/{$shipment->id}");
        $response->assertStatus(200);

        $this->assertEquals($expected, $response->json());
    }

    /**
     * Check if expected shipment can be crated
     *
     * @return void
     */
    public function test_shipment_create()
    {
        $user = $this->getAdminUser();

        $order = Order::factory()->create();
        $courier = User::factory()->create();
        $shipmentStatus = ShipmentStatus::factory()->create();
        $sendingAddress = Address::factory()->create();
        $receivingAddress = Address::factory()->create();

        $shipment = Shipment::factory()->create();
        $shipmentData = $this->getShipmentData($shipment);
        unset($shipmentData['id']);
        unset($shipmentData['order']);

        $expected = [
            'data' => $shipmentData,
            'success' => true,
            'message' => 'I18N_SHIPMENT_CREATED_SUCCESSFULLY'
        ];

        $payload = [
            'order_id' => $order->id,
            'courier_id' => $courier->id,
            'shipment_status_id' => $shipmentStatus->id,
            'sending_address_id' => $sendingAddress->id,
            'receiving_address_id' => $receivingAddress->id,
            'distance' => 3.84,
            'rate' => 2.0,
            'price' => 7.68,
            'delivery_till' => now()->toDateTimeString(),
            'delivered_at' => null
        ];

        $expected['data'] = array_merge($expected['data'], $payload);

        $response = $this->actingAs($user)->post("/api/shipments", $payload);
        $response->assertStatus(200);

        $responseData = $response->json();
        unset($responseData['data']['id']);
        unset($responseData['data']['order']);

        $this->assertEquals($expected, $responseData);
    }

    /**
     * Check if expected shipment can be updated
     *
     * @return void
     */
    public function test_shipment_update()
    {
        $user = $this->getCourierUser();

        $shipment = Shipment::factory()->create();
        $shipmentStatus = ShipmentStatus::factory()->create();

        $expected = [
            'data' => $this->getShipmentData($shipment),
            'success' => true,
            'message' => 'I18N_SHIPMENT_UPDATED_SUCCESSFULLY'
        ];

        $payload = [
            'shipment_status_id' => $shipmentStatus->id,
            'delivered_at' => now()->toDateTimeString()
        ];
        $expected['data'] = array_merge($expected['data'], $payload);

        $response = $this->actingAs($user)->put("/api/shipments/{$shipment->id}", $payload);
        $response->assertStatus(200);

        $this->assertEquals($expected, $response->json());
    }

    /**
     * Check if expected shipment can be deleted
     *
     * @return void
     */
    public function test_shipment_delete()
    {
        $user = $this->getAdminUser();

        $shipment = Shipment::factory()->create();

        $expected = [
            'data' => [],
            'success' => true,
            'message' => 'I18N_SHIPMENT_DELETED_SUCCESSFULLY'
        ];

        $response = $this->actingAs($user)->delete("/api/shipments/{$shipment->id}");
        $response->assertStatus(200);

        $this->assertEquals($expected, $response->json());
    }
}

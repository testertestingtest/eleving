<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Spatie\Permission\Models\Role;

use App\Models\User;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderStatus;

class OrderApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'order_id',
        'seller_id',
        'order_status_id',
        'tax',
        'total'
    ];

    /**
     * Retrieve Order mockup data
     *
     * @param Order $order
     * @return string[]
     */
    protected function getOrderData(Order $order): array
    {
        return [
            'id' => $order->id,
            'seller_id' => $order->seller_id,
            'customer_id' => $order->customer_id,
            'order_status_id' => $order->order_status_id,
            'tax' => $order->tax,
            'total' => $order->total,
            'products' => $order->products()->get()->toArray()
        ];
    }

    /**
     * Return user with admin role
     *
     * @return User
     */
    protected function getAdminUser(): User
    {
        $user = User::factory()->create();
        $role = Role::create(['name' => 'admin']);
        $user->assignRole([$role->id]);

        return $user;
    }

    /**
     * Check if expected orders data is the same as in API
     *
     * @return void
     */
    public function test_order_list()
    {
        $user = $this->getAdminUser();

        $expected = [
            'data' => [],
            'success' => true,
            'message' => 'I18N_ORDERS_FETCHED_SUCCESSFULLY',
        ];

        $orders = Order::factory()
            ->hasAttached(
                Product::factory()->count(3),
                ['quantity' => 1]
            )
            ->count(10)
            ->create();

        foreach($orders as $order) {
            $expected['data'][] = $this->getOrderData($order);
        }

        $response = $this->actingAs($user)->get('/api/orders');
        $response->assertStatus(200);

        $this->assertEquals($expected, $response->json());
    }

    /**
     * Check if expected order data is the same as in API
     *
     * @return void
     */
    public function test_order_get()
    {
        $user = $this->getAdminUser();

        $order = Order::factory()
            ->hasAttached(
                Product::factory()->count(3),
                ['quantity' => 1]
            )
            ->create();

        $expected = [
            'data' => $this->getOrderData($order),
            'success' => true,
            'message' => 'I18N_ORDER_FETCHED_SUCCESSFULLY'
        ];

        $response = $this->actingAs($user)->get("/api/orders/{$order->id}");
        $response->assertStatus(200);

        $this->assertEquals($expected, $response->json());
    }

    /**
     * Check if expected order can be crated
     *
     * @return void
     */
    public function test_order_create()
    {
        $user = $this->getAdminUser();

        $customer = User::factory()->create();
        $seller = User::factory()->create();
        $orderStatus = OrderStatus::factory()->create();

        $order = Order::factory()->create();
        $orderData = $this->getOrderData($order);
        unset($orderData['id']);

        $expected = [
            'data' => $orderData,
            'success' => true,
            'message' => 'I18N_ORDER_CREATED_SUCCESSFULLY'
        ];

        $payload = [
            'customer_id' => $customer->id,
            'seller_id' => $seller->id,
            'order_status_id' => $orderStatus->id,
            'tax' => 21,
            'total' => 10.01
        ];

        $expected['data'] = array_merge($expected['data'], $payload);

        $response = $this->actingAs($user)->post("/api/orders", $payload);
        $response->assertStatus(200);

        $responseData = $response->json();
        unset($responseData['data']['id']);

        $this->assertEquals($expected, $responseData);
    }

    /**
     * Check if expected order can be updated
     *
     * @return void
     */
    public function test_order_update()
    {
        $user = $this->getAdminUser();

        $order = Order::factory()->create();

        $expected = [
            'data' => $this->getOrderData($order),
            'success' => true,
            'message' => 'I18N_ORDER_UPDATED_SUCCESSFULLY'
        ];

        $payload = [
            'tax' => 21.00,
            'total' => 15.00
        ];

        $expected['data'] = array_merge($expected['data'], $payload);

        $response = $this->actingAs($user)->put("/api/orders/{$order->id}", $payload);
        $response->assertStatus(200);

        $this->assertEquals($expected, $response->json());
    }

    /**
     * Check if expected order can be deleted
     *
     * @return void
     */
    public function test_order_delete()
    {
        $user = $this->getAdminUser();

        $order = Order::factory()->create();

        $expected = [
            'data' => [],
            'success' => true,
            'message' => 'I18N_ORDER_DELETED_SUCCESSFULLY'
        ];

        $response = $this->actingAs($user)->delete("/api/orders/{$order->id}");
        $response->assertStatus(200);

        $this->assertEquals($expected, $response->json());
    }
}

<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\OrderController;
use App\Http\Controllers\ShipmentController;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\ShippingCostController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', [AuthController::class, 'signIn']);
Route::post('register', [AuthController::class, 'signUp']);

Route::middleware([ 'auth:sanctum', 'throttle:api' ])->group(function () {
    Route::apiResources([
        'orders' => OrderController::class,
        'shipments' => ShipmentController::class,
        'addresses' => AddressController::class,
    ]);

    Route::post('shipping_costs', [ShippingCostController::class, 'store']);
});

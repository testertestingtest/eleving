<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\User;
use App\Models\Order;
use App\Models\OrderStatus;

class OrderFactory extends Factory
{
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => User::factory(),
            'seller_id' => User::factory(),
            'order_status_id' => OrderStatus::factory(),
            'tax' => $this->faker->randomDigit(),
            'total' => $this->faker->randomDigit(),
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\ShipmentStatus;
use App\Models\Address;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShipmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'order_id' => Order::factory(),
            'courier_id' => User::factory(),
            'shipment_status_id' => ShipmentStatus::factory(),
            'sending_address_id' => Address::factory(),
            'receiving_address_id' => Address::factory(),
            'distance' => $this->faker->randomDigit(),
            'rate' => $this->faker->randomDigit(),
            'price' => $this->faker->randomDigit(),
            'delivery_till' => now()->toDateTimeString(),
            'delivered_at' => now()->toDateTimeString(),
        ];
    }
}

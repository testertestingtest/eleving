<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ShippingCostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'distance' => $this->faker->randomDigit(),
            'rate' => $this->faker->randomDigit(),
            'price' => $this->faker->randomDigit(),
        ];
    }
}

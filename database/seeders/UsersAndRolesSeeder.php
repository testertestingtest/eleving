<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\{ User };

class UsersAndRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Create customer
        $role = Role::create(['name' => 'customer']);
        $user = User::factory()->create(['name' => 'customer']);
        $user->assignRole([$role->id]);

        // Create sales
        $role = Role::create(['name' => 'sales']);
        $user = User::factory()->create(['name' => 'sales']);
        $user->assignRole([$role->id]);

        // Create courier
        $role = Role::create(['name' => 'courier']);
        $user = User::factory()->create(['name' => 'courier']);
        $user->assignRole([$role->id]);

        // Create admin
        $role = Role::create(['name' => 'admin']);
        $user = User::factory()->create(['name' => 'admin']);
        $user->assignRole([$role->id]);
    }
}
